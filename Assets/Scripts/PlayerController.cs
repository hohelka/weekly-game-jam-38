﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	private SideInterface leftSide;
	private SideInterface rightSide;

	// Use this for initialization
	void Start () {
		leftSide = GameObject.Find("Sztanga").transform.Find("LeftSide").GetComponent<SideInterface>();
		rightSide = GameObject.Find("Sztanga").transform.Find("RightSide").GetComponent<SideInterface>();
	}
	
	// Update is called once per frame
	void Update () {
		//if (Input.GetAxis("Horizontal") == 0) {
		//	DeepenRotation(CalculateSideBalance());
		//}
		UpdateSliders(CalculateSideBalance());
		RotatePlayer(CalculateSideBalance(), GetMouseXValue());
		CheckRotation();
	}

	void CheckRotation() {
		float angle = GetMinusedAngle(transform.parent.localEulerAngles.z);
		if (Mathf.Abs(angle) <= 10f) {
			StartLifting();
		} else {
			StopLifting();
		}
	}

	void StartLifting() {
		GetComponent<Animator>().SetBool("StopLifting", false);
	}

	void Lift() {
		GameManager.instance.UpdateScore(1f);
	}

	void StopLifting() {
		GetComponent<Animator>().SetBool("StopLifting", true);
	}

	void UpdateSliders(float balance) {
		float angle = GetMinusedAngle(transform.parent.localEulerAngles.z);
		if (angle > 0) {
			GameObject.Find("RightSideSlider").GetComponent<Slider>().value = angle;
		} else if (angle < 0) {
			GameObject.Find("LeftSideSlider").GetComponent<Slider>().value = -angle;
		}
		
	}

	float GetMinusedAngle(float z) {
		float angle = z;
 		return (angle > 180) ? angle - 360 : angle;
	}

	void RotatePlayer(float rotationBalance, float horizontalInput) {
		Quaternion targetRotation = Quaternion.Euler(new Vector3(0, 180f, rotationBalance + horizontalInput * 10f));
		transform.parent.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, Time.deltaTime * 60f);
	}

	void DeepenRotation(float rotationBalance) {
		if (rotationBalance > 0) {
			rotationBalance += rotationBalance * 0.5f / 100f;
			rightSide.SetLoad(rotationBalance);
		} else if (rotationBalance < 0) {
			rotationBalance -= rotationBalance * 0.5f / 100f;
			leftSide.SetLoad(rotationBalance);
		}
	}

	float GetHorizontalInput() {
		float horizontalInput = Input.GetAxis("Horizontal");
 		return horizontalInput * 10f;
	}

	float GetMouseXValue() {
		Vector3 v3 = Input.mousePosition;
		v3.z = 10.0f;
		v3 = Camera.main.ScreenToWorldPoint(v3);
		float mouseXValue = v3.x;
		return mouseXValue;
	}

	float CalculateSideBalance() {
		return (leftSide.GetLoad() - rightSide.GetLoad());
	}
}
