﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour {

	public List<GameObject> spawnedObjects = new List<GameObject>();
	GameObject currentlySpawnedGameObject;
	GameObject nextSpawnedGameObject;

	float timeForSpawning = 5f;
	float maxTimeToSpawn = 3f;
	float cooldownBeforeNextFallingItem = 1f;
	// Use this for initialization
	void Start () {
		currentlySpawnedGameObject = GetRandomGameObjectFromSpawnedList(spawnedObjects);
		nextSpawnedGameObject = GetRandomGameObjectFromSpawnedList(spawnedObjects);
	}
	
	// Update is called once per frame
	void Update () {
		DecreaseTimes();
		if (timeForSpawning <= 0) {
			ChangeNextSpawnedObject();
			timeForSpawning = 5f;
		} else {
			if (cooldownBeforeNextFallingItem <= 0) {
				InstantiateCurrentGameObject();
				cooldownBeforeNextFallingItem = Random.Range(1f, maxTimeToSpawn);
			}
		}
	}

	void DecreaseTimes() {
		timeForSpawning -= Time.deltaTime;
		cooldownBeforeNextFallingItem -= Time.deltaTime;
	}

	void InstantiateCurrentGameObject() {
		Instantiate(currentlySpawnedGameObject, transform.position, transform.rotation);
	}

	void ChangeNextSpawnedObject() {
		currentlySpawnedGameObject = nextSpawnedGameObject;
		nextSpawnedGameObject = GetRandomGameObjectFromSpawnedList(spawnedObjects);
	}

	GameObject GetRandomGameObjectFromSpawnedList(List<GameObject> spawnedList) {
		return spawnedList[Random.Range(0, spawnedList.Count)];
	}
}
