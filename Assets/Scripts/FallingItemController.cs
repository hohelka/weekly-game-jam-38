﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingItemController : MonoBehaviour, FallingItemInterface {

	public float fallingSpeed = 5f;
	public float weightLoad = 10f;
	public SideInterface sideInterface;
	private bool hitSide = false;
    public void Falling()
    {
        transform.Translate(-transform.up * fallingSpeed * Time.deltaTime);
    }

    public void Hit(SideInterface sideInterface)
    {
		sideInterface.GotHit(this);
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (!hitSide) {
			Falling();
		}
	}

	private void OnTriggerEnter(Collider other) {
		if (other.CompareTag("Side")) {
			Hit(other.GetComponent<SideInterface>());
			sideInterface = other.GetComponent<SideInterface>();
			transform.SetParent(sideInterface.GetGameObject().transform.parent);
			hitSide = true;
		}
		if (other.CompareTag("FallingItem") && other.GetComponent<FallingItemInterface>().GetSideInterface() != null) {
			Hit(other.GetComponent<FallingItemInterface>().GetSideInterface());
			sideInterface = other.GetComponent<FallingItemInterface>().GetSideInterface();
			transform.SetParent(sideInterface.GetGameObject().transform.parent);
			hitSide = true;
		}
	}

    public float GetWeightLoad()
    {
        return weightLoad;
    }

    public GameObject GetGameObject()
    {
        return gameObject;
    }

    public SideInterface GetSideInterface()
    {
        return sideInterface;
    }
}
