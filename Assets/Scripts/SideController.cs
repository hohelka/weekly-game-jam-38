﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideController : MonoBehaviour, SideInterface {

	public Side side;
	public SideEnum sideEnum;

	// Use this for initialization
	void Start () {
		side = new Side(sideEnum, 0f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GotHit(FallingItemInterface fallingItem)
    {
        side.load += fallingItem.GetWeightLoad();
		side.gameObjects.Add(fallingItem.GetGameObject());
    }

    public float GetLoad()
    {
        return side.load;
    }

	public GameObject GetGameObject() {
		return gameObject;
	}

    public void SetLoad(float load)
    {
        side.load = load;
    }
}
