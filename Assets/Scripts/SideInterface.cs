﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface SideInterface {

	void GotHit(FallingItemInterface fallingItem);

	float GetLoad();

	void SetLoad(float load);

	GameObject GetGameObject();
}
