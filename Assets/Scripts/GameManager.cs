﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	UnityEngine.UI.Text scoreText;
	public float score = 0;

	public static GameManager instance = null;
       void Awake()
       {
        	if (instance == null) {
               instance = this;
			} else if (instance != this) {
               Destroy(gameObject);
			return;
			}
			SceneManager.sceneLoaded += SceneLoaded;
            DontDestroyOnLoad(gameObject);
       }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void SceneLoaded(Scene scene, LoadSceneMode loadSceneMode) {
		score = 0;
		scoreText = GameObject.Find("Score").GetComponentInChildren<UnityEngine.UI.Text>();
		scoreText.text = "Score\n" + score;
	}
	public void UpdateScore(float score) {
		this.score += score;
		scoreText.text = "Score\n" + this.score;
	}
}
