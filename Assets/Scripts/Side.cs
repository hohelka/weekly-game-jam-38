﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public enum SideEnum {
	LEFT,
	RIGHT
}
[System.Serializable]
public class Side {

	public Side() {

	}

	public Side(SideEnum sideEnum, float load) {
		this.side = sideEnum;
		gameObjects = new List<GameObject>();
		this.load = load;
	}

	public SideEnum side {get;set;}
	public List<GameObject> gameObjects {get;set;}
	public float load;
}
