﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface FallingItemInterface {

	void Falling();
	void Hit(SideInterface sideInterface);

	float GetWeightLoad();
	GameObject GetGameObject();
	SideInterface GetSideInterface();
}
